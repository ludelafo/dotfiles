# dotfiles

My personal dotfiles, managed with [chezmoi](https://chezmoi.io/).

## Initialize chezmoi

Initialize the dotfiles with the following command(s).

```sh
# Initialize chezmoi
chezmoi init https://gitlab.com/ludelafo/dotfiles.git
```

## Add a file to chezmoi

Add a file to chezmoi with the following command(s).

```sh
# Add a file to chezmoi
chezmoi add <path to file(s)>

# Add a file to chezmoi with templating
chezmoi add --template <path to file(s)>
```

## Apply the chezmoi configuration

Apply the chezmoi configuration with the following command(s).

```sh
# Apply the chezmoi configuration
chezmoi apply
```

## Go to chezmoi configuration directory

Go to chezmoi configuration directory with the following command(s).

```sh
# Go to chezmoi configuration directory
chezmoi cd
```
